# CGTK

This part of port GTK+3 for Swift, use this Package only on OS X

For builing gtk-swift with this modulemap:
1. install gtk+3 through [brew](http://brew.sh/) package manager

```bash
sudo brew install gtk+3
```
2. use this command for build gtk-swift

```bash
swift build -Xlinker -L/usr/local/lib -Xcc -I/usr/local/include/cairo -Xcc -I/usr/local/include/gtk-3.0 -Xcc -I/usr/local/include/glib-2.0/ -Xcc -I/usr/local/include/pango-1.0 -Xcc -I/usr/local/include/gdk-pixbuf-2.0 -Xcc -I/usr/local/include/atk-1.0 -Xcc -I/usr/local/lib/glib-2.0/include
```

[gtk-swift](https://gitlab.com/gtk/gtk-swift/) automatically download systembase module CGTK for current environment